# Example Service

This example shows how a flask microservice for OpenPatch could be
built. It also shows how breaking changes should be handled.
Therefore the service consists of two versions. The difference
between the versions is described in the following table:

| v1 | v2 |
| -- | -- |
| /samples returns an array of sample objects | /samples returns a dict with a key samples, which points to an array of sample objects |
| Sample Schema = (id, sample_name) | Sample Schema = (id, name, value) |

API Documentation: https://openpatch.gitlab.io/flask-microservice-example

## Environment Variables

| Environment Variable | Description | Default | Values |
| -------------------- | ----------- | ------- | ------ |
| OPENPATCH_MOCK | Fill the database with mock data | not set | "true" |
| OPENPATCH_MODE | Configures the configuration | "producation" | "development", "production", "testing" |
| OPENPATCH_DB | URI for connecting to a database | sqlite:///database.db | \<protocol\>://\<path\> |
| OPENPATCH_AUTHENTIFICATION_SERVICE | url to authentification service | https://api.openpatch.app/authentification | a url |
| OPENPATCH_ORIGINS | Allowed CORS origins | "*" | valid cors origin |
| SENTRY_DSN | Error tracking with Sentry | not set | a valid dsn |

## Start

```
docker-compose up
```

## Mock Data

```
docker-compose exec backend flask mock
```

## Testing

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend flask test
```

## Coverage

```
docker-compose run --rm -e OPENPATCH_DB="sqlite+pysqlite://" -e OPENPATCH_MODE="testing" backend python3 coverage_report.py
```

## ER-Diagram

![ER-Diagram](.gitlab/er_diagram.png)
