from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_example.models.sample import Sample


class SampleSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Sample
        load_instance = True

    id = fields.UUID()
    name = ma.auto_field()
    value = ma.auto_field()


SAMPLE_SCHEMA = SampleSchema()
SAMPLES_SCHEMA = SampleSchema(many=True)

