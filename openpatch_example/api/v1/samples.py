from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_core.jwt import jwt_roles_required, jwt_required, get_jwt_claims
from openpatch_example.api.v1 import api, errors
from openpatch_example.api.v1.schemas.sample import SAMPLE_SCHEMA, SAMPLES_SCHEMA
from openpatch_example.models.sample import Sample

base_url = "/samples"


@api.route(base_url, methods=["GET"])
def get_samples():
    """
    @api {get} /v1/samples Request Samples
    @apiVersion 1.0.0
    @apiName GetSamples
    @apiGroup Samples
    """
    samples = Sample.query.all()
    return jsonify(SAMPLES_SCHEMA.dump(samples))


@api.route(base_url, methods=["POST"])
@jwt_required
def add_sample():
    """
    @api {post} /v1/samples Add Sample
    @apiVersion 1.0.0
    @apiName AddSample
    @apiGroup Samples

    @apiUse jwt

    @apiUse errors_invalid_json
    @apiUse errors_access_not_allowed
    """

    # only for demo purposes on how to get jwt claims and use custom
    # error messages. Does nothing in this example
    jwt_claims = get_jwt_claims(optional=True)
    if not jwt_claims:
        return errors.access_not_allowed()

    sample_json = request.get_json()
    try:
        sample = SAMPLE_SCHEMA.load(sample_json, session=db.session)
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(sample)
    db.session.commit()

    return jsonify({"status": "ok", "sample_id": sample.id})


@api.route(base_url + "/protected", methods=["GET"])
@jwt_roles_required(["admin"])
def protected():
    """
    @api {get} /v1/protected Protected
    @apiVersion 1.0.0
    @apiName Protected
    @apiGroup Samples

    @apiUse jwt

    @apiUse errors_invalid_json
    """
    return jsonify({}), 200
