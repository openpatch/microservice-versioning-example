from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_example.models.sample import Sample


class SampleSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Sample
        load_instance = True

    id = fields.UUID()
    sample_name = ma.auto_field(column_name="name")


SAMPLE_SCHEMA = SampleSchema()
SAMPLES_SCHEMA = SampleSchema(many=True)
