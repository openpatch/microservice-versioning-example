from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
import uuid


class Sample(Base):
    __tablename__ = gt("sample")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.Text())
    value = db.Column(db.Integer())
