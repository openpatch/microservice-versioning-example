# [2.0.0](https://gitlab.com/openpatch/microservice-versioning-example/compare/v1.0.0...v2.0.0) (2020-07-02)


### Features

* version 2 ([9e9c47d](https://gitlab.com/openpatch/microservice-versioning-example/commit/9e9c47dbb5ea39782130a9f24eff70f3c553b735))


### BREAKING CHANGES

* change db model

# 1.0.0 (2020-07-02)


### Bug Fixes

* mock invalid keyword ([369d6ef](https://gitlab.com/openpatch/microservice-versioning-example/commit/369d6ef61cacdd7e587da31d1f9a6c300ab646e7))


### Features

* version 1 ([ebe0a58](https://gitlab.com/openpatch/microservice-versioning-example/commit/ebe0a5882ebadf5d26489f5d15515a701d7a1702))
