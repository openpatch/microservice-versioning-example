from openpatch_core.database import db
from openpatch_example.models.sample import Sample


def mock():
    for i in range(10):
        db.session.add(Sample(name=f"Sample {i}", value=i))
    db.session.commit()
