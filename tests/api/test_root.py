import unittest
from tests.base_test import BaseTest


class TestRoot(BaseTest):
    def test_healthcheck(self):
        response = self.client.get("/healthcheck")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {"msg": "ok"})
