from tests.base_test import BaseTest
from openpatch_example.models.sample import Sample
import uuid


class TestSample(BaseTest):
    def test_get_samples(self):
        response = self.client.get("/v1/samples")
        samples = response.json
        self.assertEqual(len(samples), 10)

        for sample in samples:
            self.assertIn("id", sample)
            self.assertIn("sample_name", sample)

    def test_add_sample_no_token(self):
        response = self.client.post("/v1/samples", json={},)
        self.assertEqual(response.status_code, 403)
        self.assertIn("message", response.json)
        self.assertEqual(response.json.get("code"), 1)

    def test_add_sample_empty(self):
        response = self.client.post(
            "/v1/samples", json={}, headers=self.fake_headers["admin"],
        )
        self.assertTrue("status" in response.json)
        self.assertEqual(response.json.get("status"), "ok")
        self.assertEqual(Sample.query.count(), 11)

    def test_add_sample_malformed(self):
        response = self.client.post(
            "/v1/samples", json={"id": 12}, headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 400)
        self.assertIn("message", response.get_json())
        self.assertIn("details", response.get_json())
        self.assertEqual(Sample.query.count(), 10)

    def test_add_sample_full(self):
        id = str(uuid.uuid4())
        response = self.client.post(
            "/v1/samples",
            json={"id": id, "sample_name": "Hu"},
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("status", response.json)
        self.assertEqual(response.json.get("status"), "ok")
        self.assertIn("sample_id", response.json)
        sample = Sample.query.get(response.json.get("sample_id"))
        self.assertIsNotNone(sample)
        self.assertEqual(sample.name, "Hu")
        self.assertEqual(str(sample.id), id)

    def test_protected_no_token(self):
        response = self.client.get("/v1/samples/protected")
        self.assertEqual(response.status_code, 401)

    def test_protected_valid_token(self):
        response = self.client.get(
            "/v1/samples/protected", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)
